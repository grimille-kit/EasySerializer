package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import fr.grimille.kit.Utils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.List;

public class PotionAdapter extends DefaultAdapter {
    private static final String KEY_BASE = "base";
    private static final String KEY_COLOR = "color";
    private static final String KEY_EFFECTS = "potion_effects";

    public PotionAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final PotionMeta meta = (PotionMeta)parent.getItemMeta();
        final List<PotionEffect> potionEffects = deserializeEffects();

        meta.setColor(Utils.parseColor(section.getString(KEY_COLOR)));
        meta.setBasePotionData(new PotionData(PotionType.valueOf(section.getString(KEY_BASE + ".type")), section.getBoolean(KEY_BASE + ".extended"), section.getBoolean(KEY_BASE + ".upgraded")));
        potionEffects.forEach(effect -> meta.addCustomEffect(effect, true));
        parent.setItemMeta(meta);

        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);
        final PotionMeta meta = (PotionMeta)is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.POTION.getTypeName());
        section.set(KEY_COLOR, Utils.toColorString(meta.getColor()));
        section.set(KEY_BASE + ".type", meta.getBasePotionData().getType().name());
        section.set(KEY_BASE + ".extended", meta.getBasePotionData().isExtended());
        section.set(KEY_BASE + ".upgraded", meta.getBasePotionData().isUpgraded());

        serializeEffects(meta.getCustomEffects());
    }

    private void serializeEffects(List<PotionEffect> potionEffects) {
        int counter = 0;
        for(PotionEffect effect : potionEffects) {
            counter++;
            section.set(KEY_EFFECTS + "." + String.valueOf(counter) + ".type", effect.getType().getName());
            section.set(KEY_EFFECTS + "." + String.valueOf(counter) + ".amplifier", effect.getAmplifier());
            section.set(KEY_EFFECTS + "." + String.valueOf(counter) + ".duration", effect.getDuration());
            section.set(KEY_EFFECTS + "." + String.valueOf(counter) + ".ambiant", effect.isAmbient());
            section.set(KEY_EFFECTS + "." + String.valueOf(counter) + ".particles", effect.hasParticles());
            section.set(KEY_EFFECTS + "." + String.valueOf(counter) + ".color", Utils.toColorString(effect.getColor()));
        }
    }

    private List<PotionEffect> deserializeEffects() {
        final List<PotionEffect> potionEffects = new ArrayList<>();
        final ConfigurationSection potionEffectsSection = section.getConfigurationSection(KEY_EFFECTS);

        if(potionEffectsSection != null) {
            for(String index : potionEffectsSection.getKeys(false)) {
                potionEffects.add(new PotionEffect(PotionEffectType.getByName(potionEffectsSection.getString(index + ".type")), potionEffectsSection.getInt(index + ".duration"), potionEffectsSection.getInt(index + ".amplifier"), potionEffectsSection.getBoolean(index + ".ambient"), potionEffectsSection.getBoolean(index + ".particles"), Utils.parseColor(potionEffectsSection.getString(index + ".color"))));
            }
        }
        return potionEffects;
    }
}
