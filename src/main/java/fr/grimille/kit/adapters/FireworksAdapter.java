package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import fr.grimille.kit.Utils;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.ArrayList;
import java.util.List;

public class FireworksAdapter extends DefaultAdapter {
    private static final String KEY_POWER = "power";
    private static final String KEY_EFFECTS = "effects";

    public FireworksAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final FireworkMeta meta = (FireworkMeta)parent.getItemMeta();

        meta.setPower(section.getInt(KEY_POWER));
        meta.addEffects(deserializeEffects());

        parent.setItemMeta(meta);
        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);
        final FireworkMeta meta = (FireworkMeta)is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.FIREWORKS.getTypeName());
        section.set(KEY_POWER, meta.getPower());

        serializeFireworkEffects(meta.getEffects());
    }

    /**
     * Serialize all the firework effects for this ItemStack
     * @param fwEffects - The Fireworks Effect list to serialize.
     */
    private void serializeFireworkEffects(List<FireworkEffect> fwEffects) {
        ConfigurationSection effectTypeSection;

        int counter = 0;
        for(FireworkEffect effect : fwEffects) {
            counter++;
            final List<String> colors = new ArrayList<>();
            final List<String> fades = new ArrayList<>();

            effect.getColors().forEach(color -> colors.add(Utils.toColorString(color)));
            effect.getFadeColors().forEach(color -> fades.add(Utils.toColorString(color)));
            effectTypeSection = section.createSection(KEY_EFFECTS + "." + String.valueOf(counter));
            effectTypeSection.set("colors", colors);
            effectTypeSection.set("fades", fades);
            effectTypeSection.set("type", effect.getType().name());
            effectTypeSection.set("flicker", effect.hasFlicker());
            effectTypeSection.set("trail", effect.hasTrail());
        }
    }

    /**
     * Deserialize the fireworks effects to this ItemStack
     * @return - The Fireworks Effect list of the Firework itemstack.
     */
    private List<FireworkEffect> deserializeEffects() {
        final List<FireworkEffect> effects = new ArrayList<>();
        final ConfigurationSection effectsSection = section.getConfigurationSection(KEY_EFFECTS);

        FireworkEffect fireworkEffect;
        FireworkEffect.Type fwType;
        List<String> colors;
        List<String> fades;
        boolean flicker;
        boolean trail;

        if(effectsSection != null) {
            for(String key : effectsSection.getKeys(false)) {
                final List<Color> colorList = new ArrayList<>();
                final List<Color> fadeList = new ArrayList<>();

                colors = effectsSection.getStringList(key + ".colors");
                fades = effectsSection.getStringList(key + ".fades");
                fwType = FireworkEffect.Type.valueOf(effectsSection.getString(key + ".type"));
                flicker = effectsSection.getBoolean(key + ".flicker");
                trail = effectsSection.getBoolean(key + ".trail");
                colors.forEach(color -> colorList.add(Utils.parseColor(color)));
                fades.forEach(color -> fadeList.add(Utils.parseColor(color)));
                fireworkEffect = FireworkEffect.builder()
                        .flicker(flicker)
                        .trail(trail)
                        .withColor(colorList)
                        .withFade(fadeList)
                        .with(fwType)
                        .build();

                effects.add(fireworkEffect);
            }
        }

        return effects;
    }
}
