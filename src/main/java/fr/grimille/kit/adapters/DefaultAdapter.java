package fr.grimille.kit.adapters;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class DefaultAdapter {
    private static final String KEY_MATERIAL = "material";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_DURABILITY = "durability";
    private static final String KEY_DISPLAY_NAME = "display_name";
    private static final String KEY_LORE = "lore";
    private static final String KEY_LOCALE = "locale";
    private static final String KEY_ENCHANTS = "enchants";
    private static final String KEY_UNBREAKABLE = "unbreakable";
    private static final String KEY_FLAGS = "flags";
    protected static final String KEY_TYPE = "type";

    protected final ConfigurationSection section;

    public DefaultAdapter(ConfigurationSection section) {
        this.section = section;
    }

    /**
     * Read the itemstack localized at the given configuration section.
     * @return - The itemstack freshly read. :)
     */
    public ItemStack read() {
        String materialName, title, locale;
        int amount;
        short durability;
        Material material;
        List<String> lore, flags;
        Map<Enchantment, Integer> enchants;
        boolean unbreakable;

        materialName = section.getString(KEY_MATERIAL) != null ? section.getString(KEY_MATERIAL) : "AIR";
        amount = section.getInt(KEY_AMOUNT) != 0 ? section.getInt(KEY_AMOUNT) : 1;
        durability = section.getInt(KEY_DURABILITY) != 0 ? (short)section.getInt(KEY_DURABILITY) : 0;

        title = section.getString(KEY_DISPLAY_NAME);
        locale = section.getString(KEY_LOCALE);
        lore = section.getStringList(KEY_LORE);
        flags = section.getStringList(KEY_FLAGS);
        unbreakable = section.getBoolean(KEY_UNBREAKABLE);
        enchants = deserializeEnchantments();

        material = Material.getMaterial(materialName) != null ? Material.getMaterial(materialName) : Material.AIR;

        final ItemStack is = new ItemStack(material, amount, durability);
        final ItemMeta meta = is.getItemMeta();

        meta.setDisplayName(title);
        meta.setLocalizedName(locale);
        meta.setLore(lore);
        meta.setUnbreakable(unbreakable);
        meta.addItemFlags(deserializeFlags(flags));

        is.setItemMeta(meta);
        is.addUnsafeEnchantments(enchants);

        return is;
    }

    /**
     * Write an itemstack to the configuration section.
     * @param is - The itemstack to write.
     */
    public void write(ItemStack is) {
       section.set(KEY_MATERIAL, is.getType().name());
       section.set(KEY_AMOUNT, is.getAmount());
       section.set(KEY_DURABILITY, is.getDurability());

       if(is.hasItemMeta()) {
           final ItemMeta meta = is.getItemMeta();
           final Set<ItemFlag> flags = meta.getItemFlags();

           if(meta.hasDisplayName()) section.set(KEY_DISPLAY_NAME, meta.getDisplayName());
           if(meta.hasLore()) section.set(KEY_LORE, meta.getLore());
           if(meta.hasLocalizedName()) section.set(KEY_LOCALE, meta.getLocalizedName());
           if(meta.isUnbreakable()) section.set(KEY_UNBREAKABLE, meta.isUnbreakable());
           if(meta.hasEnchants()) serializeEnchants(meta.getEnchants());
           if(!flags.isEmpty()) serializeFlags(flags);
       }
    }

    /**
     * Serialize enchants in a nice way.
     * @param enchants - The enchantment map to serialize.
     */
    private void serializeEnchants(Map<Enchantment, Integer> enchants) {
        enchants.keySet().forEach(
                enchant -> section.set(KEY_ENCHANTS + "." + enchant.getName().toLowerCase(), enchants.get(enchant))
        );
    }

    /**
     * Serialize item flags in a nice way.
     * @param flags - The flags list to serialize.
     */
    private void serializeFlags(Set<ItemFlag> flags) {
        final List<String> flagsStr = new ArrayList<>();

        flags.forEach(flag -> flagsStr.add(flag.name()));
        section.set(KEY_FLAGS, flagsStr);
    }

    /**
     * Deserialize the enchantment map from our nice storage form.
     * @return - The enchantment map from our section.
     */
    private Map<Enchantment, Integer> deserializeEnchantments() {
        final Map<Enchantment, Integer> enchants = new HashMap<>();
        final ConfigurationSection enchantSection = section.getConfigurationSection(KEY_ENCHANTS);

        if(enchantSection != null) {
            for(String enchantKey : enchantSection.getKeys(false)) {
                enchants.put(Enchantment.getByName(enchantKey.toUpperCase()), enchantSection.getInt(enchantKey));
            }
        }

        return enchants;
    }

    /**
     * Deserialize the item flags list from our nice storage form.
     * @return - The item flags list from our section.
     */
    private ItemFlag[] deserializeFlags(List<String> itemFlags) {
        final ItemFlag[] flags = new ItemFlag[itemFlags.size()];

        for(int i = 0; i < flags.length; i++) {
            flags[i] = ItemFlag.valueOf(itemFlags.get(i));
        }

        return flags;
    }
}
