package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import org.bukkit.DyeColor;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends DefaultAdapter {
    private static final String KEY_PATTERNS = "patterns";

    public BannerAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final BannerMeta meta = (BannerMeta)parent.getItemMeta();
        final List<Pattern> patterns = deserializePatterns();

        meta.setPatterns(patterns);
        parent.setItemMeta(meta);

        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);

        final BannerMeta meta = (BannerMeta)is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.BANNER.getTypeName());
        serializePatterns(meta.getPatterns());
    }

    /**
     * Serialize patterns for this banner itemstack
     * @param patterns - The patterns of the banner.
     */
    private void serializePatterns(List<Pattern> patterns) {
        Pattern currentPattern;

        for(int i = 0; i < patterns.size(); i++) {
            currentPattern = patterns.get(i);
            section.set(KEY_PATTERNS + "." + String.valueOf(i) + "." + currentPattern.getPattern().name().toLowerCase(), currentPattern.getColor().name());
        }
    }

    /**
     * Deserialize the patterns to a banner itemstack.
     * @return - The list of the Pattern to apply to the Banner.
     */
    private List<Pattern> deserializePatterns() {
        final ConfigurationSection patternSection = section.getConfigurationSection(KEY_PATTERNS);
        final List<Pattern> patterns = new ArrayList<>();

        ConfigurationSection indexSection;

        if(patternSection != null) {
            for(String index : patternSection.getKeys(false)) {
                indexSection = patternSection.getConfigurationSection(index);

                if(indexSection != null) {
                    for(String patternType : indexSection.getKeys(false)) {
                        patterns.add(new Pattern(DyeColor.valueOf(indexSection.getString(patternType)), PatternType.valueOf(patternType.toUpperCase())));
                    }
                }
            }
        }
        return patterns;
    }
}
