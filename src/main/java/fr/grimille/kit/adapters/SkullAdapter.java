package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

public class SkullAdapter extends DefaultAdapter {
    private static final String KEY_UUID = "uuid";
    private static final String KEY_PLAYERNAME = "name";

    public SkullAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final SkullMeta meta = (SkullMeta)parent.getItemMeta();

        try {
            meta.setOwningPlayer(Bukkit.getOfflinePlayer(UUID.fromString(section.getString(KEY_UUID))));
        } catch(NoSuchMethodError | NullPointerException e) { //1.12.2 now require OfflinePlayer
            meta.setOwner(section.getString(KEY_PLAYERNAME));
        }
        parent.setItemMeta(meta);
        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);

        final SkullMeta meta = (SkullMeta)is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.SKULL.getTypeName());

        try {
            section.set(KEY_UUID, meta.getOwningPlayer().getUniqueId());
        } catch(NoSuchMethodError e) { //1.12.2 now require OfflinePlayer
            section.set(KEY_PLAYERNAME, meta.getOwner());
        }
    }
}
