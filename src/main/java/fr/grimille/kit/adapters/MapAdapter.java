package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import fr.grimille.kit.Utils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.MapMeta;

public class MapAdapter extends DefaultAdapter {
    private static final String KEY_COLOR = "map_color";
    private static final String KEY_SCALING = "scaling";
    private static final String KEY_LOCATION_NAME = "location_name";
    public MapAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final MapMeta meta = (MapMeta)parent.getItemMeta();

        meta.setColor(Utils.parseColor(section.getString(KEY_COLOR)));
        meta.setScaling(section.getBoolean(KEY_SCALING));
        meta.setLocationName(section.getString(KEY_LOCATION_NAME));
        parent.setItemMeta(meta);

        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);
        final MapMeta meta = (MapMeta)is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.MAP.getTypeName());
        section.set(KEY_SCALING, meta.isScaling());
        section.set(KEY_LOCATION_NAME, meta.getLocationName());
        section.set(KEY_COLOR, Utils.toColorString(meta.getColor()));
    }
}
