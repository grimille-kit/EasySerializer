package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SpawnEggMeta;

public class SpawnEggAdapter extends DefaultAdapter {
    private static final String KEY_ENTITY = "entity";

    public SpawnEggAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final SpawnEggMeta meta = (SpawnEggMeta)parent.getItemMeta();

        meta.setSpawnedType(EntityType.valueOf(section.getString(KEY_ENTITY)));

        parent.setItemMeta(meta);
        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);

        final SpawnEggMeta meta = (SpawnEggMeta)is.getItemMeta();
        section.set(KEY_TYPE, AdaptersMapping.EGG.getTypeName());
        section.set(KEY_ENTITY, meta.getSpawnedType().name());
    }
}
