package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import fr.grimille.kit.Utils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class LeatherAdapter extends DefaultAdapter {
    private static final String KEY_LEATHER_COLOR = "leather_color";

    public LeatherAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final LeatherArmorMeta meta = (LeatherArmorMeta)parent.getItemMeta();

        meta.setColor(Utils.parseColor(section.getString(KEY_LEATHER_COLOR)));
        parent.setItemMeta(meta);

        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);
        final LeatherArmorMeta meta = (LeatherArmorMeta)is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.LEATHER.getTypeName());
        section.set(KEY_LEATHER_COLOR, Utils.toColorString(meta.getColor()));
    }
}
