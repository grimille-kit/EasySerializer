package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.HashMap;
import java.util.Map;

public class EnchantStorageAdapter extends DefaultAdapter {
    private static final String KEY_ENCHANTS = "stored_enchants";

    public EnchantStorageAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent = super.read();
        final EnchantmentStorageMeta meta = (EnchantmentStorageMeta)parent.getItemMeta();
        final Map<Enchantment, Integer> enchants = deserializeStoredEnchants();

        for(Enchantment enchantment : enchants.keySet()) {
            meta.addStoredEnchant(enchantment, enchants.get(enchantment), true);
        }

        parent.setItemMeta(meta);

        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);
        final EnchantmentStorageMeta meta = (EnchantmentStorageMeta) is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.ENCHANTED_STORAGE.getTypeName());

        serializeStoredEnchants(meta.getStoredEnchants());
    }

    /**
     * Serialize stored enchantments on this Itemstack.
     * @param enchants - The enchants to serialize.
     */
    private void serializeStoredEnchants(Map<Enchantment, Integer> enchants) {
        for(Enchantment enchantment : enchants.keySet()) {
            section.set(KEY_ENCHANTS + "." + enchantment.getName().toLowerCase(), enchants.get(enchantment));
        }
    }

    /**
     * Deserialize stored enchantments to this ItemStack.
     * @return - The enchantment map of the itemstack.
     */
    private Map<Enchantment, Integer> deserializeStoredEnchants() {
        final Map<Enchantment, Integer> enchants = new HashMap<>();
        final ConfigurationSection enchantSection = section.getConfigurationSection(KEY_ENCHANTS);

        if(enchantSection != null) {
            for(String enchantStr : enchantSection.getKeys(false)) {
                enchants.put(Enchantment.getByName(enchantStr.toUpperCase()), enchantSection.getInt(enchantStr));
            }
        }
        return enchants;
    }
}
