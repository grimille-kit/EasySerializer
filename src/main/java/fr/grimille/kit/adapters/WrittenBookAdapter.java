package fr.grimille.kit.adapters;

import fr.grimille.kit.AdaptersMapping;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class WrittenBookAdapter extends DefaultAdapter {
    private static final String KEY_TITLE = "title";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_PAGES = "pages";
    private static final String KEY_GENERATION = "generation";

    public WrittenBookAdapter(ConfigurationSection section) {
        super(section);
    }

    @Override
    public ItemStack read() {
        final ItemStack parent =  super.read();
        final BookMeta meta = (BookMeta) parent.getItemMeta();

        meta.setTitle(section.getString(KEY_TITLE));
        meta.setAuthor(section.getString(KEY_AUTHOR));
        meta.setPages(section.getStringList(KEY_PAGES));
        meta.setGeneration(BookMeta.Generation.valueOf(section.getString(KEY_GENERATION)));

        parent.setItemMeta(meta);

        return parent;
    }

    @Override
    public void write(ItemStack is) {
        super.write(is);
        final BookMeta meta = (BookMeta) is.getItemMeta();

        section.set(KEY_TYPE, AdaptersMapping.WRITTEN_BOOK.getTypeName());
        section.set(KEY_TITLE, meta.getTitle());
        section.set(KEY_AUTHOR, meta.getAuthor());
        section.set(KEY_PAGES, meta.getPages());
        section.set(KEY_GENERATION, meta.getGeneration().name());
    }
}
