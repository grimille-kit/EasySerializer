package fr.grimille.kit;

import fr.grimille.kit.adapters.*;
import org.bukkit.inventory.meta.*;

/**
 * This class is the mapping for all item meta type to  their
 * respective meta serializer class. The mapping class is instanciate via reflection.
 */
public enum AdaptersMapping {
    DEFAULT("default", DefaultAdapter.class, ItemMeta.class),
    BANNER("banner", BannerAdapter.class, BannerMeta.class),
    ENCHANTED_STORAGE("enchanted_storage", EnchantStorageAdapter.class, EnchantmentStorageMeta.class),
    FIREWORKS("fireworks", FireworksAdapter.class, FireworkMeta.class),
    LEATHER("leather", LeatherAdapter.class, LeatherArmorMeta.class),
    MAP("map", MapAdapter.class, MapMeta.class),
    POTION("potion", PotionAdapter.class, PotionMeta.class),
    SKULL("skull", SkullAdapter.class, SkullMeta.class),
    EGG("egg", SpawnEggAdapter.class, SpawnEggMeta.class),
    WRITTEN_BOOK("written_book", WrittenBookAdapter.class, BookMeta.class);

    private String typeName;
    private Class<? extends DefaultAdapter> adapterClass;
    private Class<? extends ItemMeta> metaClass;

    AdaptersMapping(String typeName, Class<? extends DefaultAdapter> adapterClass, Class<? extends ItemMeta> metaClass) {
        this.typeName = typeName;
        this.adapterClass = adapterClass;
        this.metaClass = metaClass;
    }

    /**
     * Get the adapter class from the type name.
     * @param typeName - The type name of the Item Meta.
     * @return - The Adapter class of the Item Meta or Default Adapter.
     */
    public static Class<? extends DefaultAdapter> getAdapterClassByType(String typeName) {
        for(AdaptersMapping mapping : AdaptersMapping.values()) {
            if(mapping.typeName.equals(typeName)) {
                return mapping.adapterClass;
            }
        }
        return AdaptersMapping.DEFAULT.adapterClass;
    }

    /**
     * Get the meta class from the type name.
     * @param typeName - The type name of the Item Meta.
     * @return - The meta class of the Item Meta.
     */
    public static Class<? extends ItemMeta> getMetaClassByType(String typeName) {
        for(AdaptersMapping mapping : AdaptersMapping.values()) {
            if(mapping.typeName.equals(typeName)) {
                return mapping.metaClass;
            }
        }
        return AdaptersMapping.DEFAULT.metaClass;
    }

    /**
     * Get the adapter mapping from the meta class.
     * @param metaClass - The meta class of the Item Meta.
     * @return - The mapping associated to the meta class or default mapping.
     */
    public static AdaptersMapping getMappingByMetaClass(Class<? extends ItemMeta> metaClass) {
        for(AdaptersMapping mapping : AdaptersMapping.values()) {
            if(mapping.metaClass.equals(metaClass.getInterfaces()[0])) {
                return mapping;
            }
        }
        return AdaptersMapping.DEFAULT;
    }

    public String getTypeName() {
        return typeName;
    }

    public Class<? extends DefaultAdapter> getAdapterClass() {
        return adapterClass;
    }
}
