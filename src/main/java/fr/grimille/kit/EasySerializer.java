/**
 * This class is a static helper which will help you
 * to serialize and deserialize MC ItemStacks or Inventories to your
 * system storage. It takes into account all convenient metas of an ItemStack and
 * create a fancy way to serialize/deserialize it.
 */
package fr.grimille.kit;

import fr.grimille.kit.adapters.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class EasySerializer {
    /**
     * Serialize the itemstack given in parameter to a configuration section also given in parameter.
     * @param section - The section you wish to save your itemstack.
     * @param is - The itemstack you want to save.
     */
    public static void serializeItemStack(ConfigurationSection section, ItemStack is) {
        final AdaptersMapping mapping = AdaptersMapping.getMappingByMetaClass(is.getItemMeta().getClass());
        final Class<? extends DefaultAdapter> adapterClass = mapping.getAdapterClass();

        try {
            final Constructor constructor = adapterClass.getDeclaredConstructor(ConfigurationSection.class);
            final DefaultAdapter adapter = (DefaultAdapter)constructor.newInstance(section);

            adapter.write(is);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deserialize the configuration section to an Itemstack.
     * @param section - The section you wish to get your itemstack.
     */
    public static ItemStack deserializeItemStack(ConfigurationSection section) {
        final String type = section.getString("type");
        final Class<? extends DefaultAdapter> adapterClass = AdaptersMapping.getAdapterClassByType(type);
        try {
            final Constructor constructor = adapterClass.getDeclaredConstructor(ConfigurationSection.class);
            final DefaultAdapter adapter = (DefaultAdapter)constructor.newInstance(section);

            return adapter.read();
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Serialize an entire inventory to the given section.
     * @param section - The section in the yaml where you want to serialize the inventory.
     * @param inventory - The inventory you wanna save. ;)
     */
    public static void serializeInventory(ConfigurationSection section, Inventory inventory) {
        ItemStack is;
        for(int i = 0; i < inventory.getSize(); i++) {
            is = inventory.getItem(i);

            if(is != null) {
                serializeItemStack(section.createSection("slot-" + i), is);
            }
        }
    }

    /**
     * Deserialize an entire inventory from the given section.
     * @param section - The section in the yaml containing the inventory you want to deserialize.
     * @param inventory - The inventory you wanna get again. :)
     */
    public static void deserializeInventory(ConfigurationSection section, Inventory inventory) {
        ConfigurationSection slotSection;
        for(int i = 0; i < inventory.getSize(); i++) {
            slotSection = section.getConfigurationSection("slot-" + i);
            if(slotSection != null) {
                inventory.setItem(i, deserializeItemStack(slotSection));
            }
        }
    }
}
