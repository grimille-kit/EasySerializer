package fr.grimille.kit;

import org.bukkit.Color;

public class Utils {
    /**
     * Parse a serialized color.
     * @param color - The color string to parse.
     * @return - The Color RGB of the color string.
     */
    public static Color parseColor(String color) {
        if(color != null) {
            final String[] colorValues = color.split(";");

            if(colorValues.length == 3) {
                return Color.fromRGB(Integer.parseInt(colorValues[0]),Integer.parseInt(colorValues[1]),Integer.parseInt(colorValues[2]));
            }
            return Color.WHITE;
        }
        return null;
    }

    /**
     * Convert a color to his String representation.
     * @param color - The color to convert
     * @return - The color in String.
     */
    public static String toColorString(Color color) {
        if(color == null) {
            return null;
        }
        return color.getRed() + ";" + color.getGreen() + ";" + color.getBlue();
    }
}
