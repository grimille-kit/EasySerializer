**EasySerializer est une lib qui permet d'enregistrer et de récupérer des items de Minecraft dans des fichiers YML en une ligne de code !**


Utilisez la classe EasySerializer.serialize() pour enregistrer des inventaires ou un item, et EasySerializer.deserialize() pour récupérer un inventaire ou un item.

EasySerializer sauvegarde chaque meta d'items ainsi que les enchantements pour les versions inférieures ou égales à MC 1.12.2.
Pour les versions ultérieures, le code n'a pas été testé mais pourrait fonctionner sans mise à jour de la lib.